import logging

from .screens import Screens

LOG = logging.getLogger(__name__)


class UIStateMachine:
    SINGLETON = None

    def __new__(cls, *args, **kwargs):
        if cls.SINGLETON is not None:
            LOG.debug('returning existing UIStateMachine')
            return cls.SINGLETON
        LOG.debug('creating new UIStateMachine')
        cls.SINGLETON = super().__new__(cls)
        return cls.SINGLETON

    def __init__(self):
        LOG.debug('initializing UI state machine with title')
        self.screen = Screens.title
        self.state = None

    def run(self):
        LOG.debug('running UI state machine')
        while self.screen is not None:
            screen_cls = self.screen.get()
            if screen_cls is None:
                LOG.warning(f'no implementation for screen {self.screen!r}')
                break
            LOG.debug(f'run screen {screen_cls!r} with state {self.state!r}')
            self.screen, self.state = screen_cls.run(self.state)
            LOG.debug(f'got state {self.state}')
