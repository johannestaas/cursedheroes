import logging

from ezcurses import Cursed

from . import Menu, Screens, get_input
from ..map import Map

LOG = logging.getLogger(__name__)


class MapEditorScreen:

    @classmethod
    def run(cls, state):
        LOG.debug('running map editor')
        with Cursed() as scr:
            width = get_input(
                scr, 'width? ', validator='pos_int',
            )
            height = get_input(
                scr, 'height? ', pos=(0, 1), validator='pos_int',
            )
            LOG.debug(f'width and height are: {width!r}, {height!r}')
            map = Map(map=None, name=None, width=width, height=height)
            map.show_minimap(scr)
        return None, None
