import logging

from ezcurses import Cursed

from . import Menu, Screens

LOG = logging.getLogger(__name__)


class TitleMenu(Menu):
    new = '[n]ew game'
    load = '[l]oad game'
    map_editor = '[m]ap editor'
    quit = '[q]uit'


class TitleScreen:

    @classmethod
    def run(cls, state):
        LOG.debug('running Cursed title screen')
        with Cursed() as scr:
            win = scr.new_win(orig=(0, 0), size=scr.max_size())
            item = TitleMenu.show(win)
            LOG.debug(f'selected {item!r}')
            if item is TitleMenu.new:
                return Screens.new_game, None
            elif item is TitleMenu.load:
                return Screens.load_game, None
            elif item is TitleMenu.map_editor:
                return Screens.map_editor, None
            else:
                return None, None
