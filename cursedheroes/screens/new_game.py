import logging

from ezcurses import Cursed

from ..map import Map

LOG = logging.getLogger(__name__)
LEVELS = []


class NewGameScreen:

    @classmethod
    def run(cls, state):
        LOG.debug('running Cursed new game screen')
        with Cursed() as scr:
            win = scr.new_win(orig=(0, 0), size=scr.max_size())
            map = Map.map_menu(win)
            LOG.info(f'selected {map.name}')
            scr.refresh()
            map.show_minimap(win)
            return None, None
