'''
cursedheroes

A turn-based strategy game in your terminal!
'''

__title__ = 'cursedheroes'
__version__ = '0.0.1'
__all__ = ()
__author__ = 'Johan Nestaas <johannestaas@gmail.com'
__license__ = 'GPLv3'
__copyright__ = 'Copyright 2019 Johan Nestaas'

# Keep LOG the first import to initialize it.
from .log import LOG
from .main import main
