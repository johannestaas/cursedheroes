import logging

LOG = None


def setup_logging(
    log_path='./cursedheroes.log',
    file_level='info',
    stream=False,
    stream_level='error',
):
    global LOG
    LOG = logging.getLogger('cursedheroes')
    LOG.setLevel(logging.DEBUG)

    if log_path:
        file_handler = logging.FileHandler(log_path)
        file_handler.setLevel(getattr(logging, file_level.upper()))

    if stream:
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(getattr(logging, stream_level.upper()))

    formatter = logging.Formatter(
        '%(asctime)s::%(name)s [%(levelname)s] %(message)s'
    )

    if log_path:
        file_handler.setFormatter(formatter)
        LOG.addHandler(file_handler)

    if stream:
        stream_handler.setFormatter(formatter)
        LOG.addHandler(stream_handler)
