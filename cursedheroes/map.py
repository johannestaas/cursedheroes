import os
import json
import logging

LOG = logging.getLogger(__name__)

ROOT_DIR = os.path.dirname(__file__)
MAP_DIR = os.path.join(ROOT_DIR, 'data', 'maps')


class Tile:

    def __init__(self, terrain=None):
        self.terrain = terrain

    def __str__(self):
        return {
            None: '?',
        }[self.terrain]


class Map:
    MAPS = []

    @staticmethod
    def _blank_map(width, height):
        map = []
        for y in range(height):
            row = []
            for x in range(width):
                row.append(Tile())
            map.append(row)
        return map

    def __init__(self, map=None, name=None, width=0, height=0):
        if map is None:
            map = Map._blank_map(width, height)
        self.map = map
        self.name = name
        self.width = width
        self.height = height

    def __repr__(self):
        return f'Map(name={self.name!r}, size=({self.width}, {self.height}))'

    @classmethod
    def load_maps(cls):
        map_fns = os.listdir(MAP_DIR)
        for fn in map_fns:
            if os.path.splitext(fn)[1] != '.json':
                continue
            path = os.path.join(MAP_DIR, fn)
            Map.load_meta(path)

    @classmethod
    def load_meta(cls, path):
        with open(path) as f:
            data = json.load(f)
        map = Map(**data)
        cls.MAPS.append(map)
        LOG.debug(f'Loaded map {map!r}')

    @classmethod
    def list_maps(cls):
        return cls.MAPS

    @classmethod
    def map_menu(cls, scr):
        msgs = []
        width, height = scr.max_size()
        menu_win = scr.new_win(orig=(0, 1), size=(width, height - 1))
        for i, map in enumerate(cls.list_maps()):
            msgs.append((None, map.name))
        scr.write('Select Map', pos=(0, 0))
        scr.refresh()
        idx = None
        while idx is None:
            idx = menu_win.get_menu_item(msgs, orig=(0, 0))
        return cls.MAPS[idx]

    def show_minimap(self, scr):
        scr.write(f'Name: {self.name}', pos=(0, 0))
        scr.write(f'Size: {self.width}x{self.height}', pos=(0, 1))
        for y, row in enumerate(self.map):
            for x, col in enumerate(row):
                scr.write(str(col), pos=(x, y + 2))
        scr.getch()
