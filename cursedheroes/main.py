import logging

from .log import setup_logging
from .map import Map
from .ui import UIStateMachine

LOG = logging.getLogger(__name__)


def initialize():
    LOG.debug('loading maps...')
    Map.load_maps()
    LOG.debug('loaded.')
    ui = UIStateMachine()
    ui.run()


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--log-path', '-l', default='./cursedheroes.log',
        help='path to log to, default: %(default)s',
    )
    parser.add_argument(
        '--log-file-level', '-f', default='debug',
        help='file log level, default: %(default)s',
    )
    parser.add_argument(
        '--log-stdout', '-L', action='store_true',
        help='whether to log to stdout, default: %(default)s',
    )
    parser.add_argument(
        '--log-stdout-level', '-s', default='error',
        help='stdout log level, default: %(default)s',
    )
    args = parser.parse_args()
    setup_logging(
        log_path=args.log_path,
        file_level=args.log_file_level,
        stream=args.log_stdout,
        stream_level=args.log_stdout_level,
    )
    initialize()
