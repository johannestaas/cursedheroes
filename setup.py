import os
from setuptools import setup

# cursedheroes
# A turn-based strategy game in your terminal!


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="cursedheroes",
    version="0.0.1",
    description="A turn-based strategy game in your terminal!",
    author="Johan Nestaas",
    author_email="johannestaas@gmail.com",
    license="GPLv3",
    keywords="tbs turn-based strategy turn based game",
    url="https://bitbucket.org/johannestaas/cursedheroes",
    packages=['cursedheroes'],
    package_dir={'cursedheroes': 'cursedheroes'},
    long_description=read('README.rst'),
    classifiers=[
        # 'Development Status :: 1 - Planning',
        # 'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        # 'Development Status :: 4 - Beta',
        # 'Development Status :: 5 - Production/Stable',
        # 'Development Status :: 6 - Mature',
        # 'Development Status :: 7 - Inactive',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
    ],
    install_requires=[
        'ezcurses',
    ],
    entry_points={
        'console_scripts': [
            'cursedheroes=cursedheroes:main',
        ],
    },

    zip_safe=False,
    package_data={
        'cursedheroes': [
            'data/maps/*.json',
        ],
    },
    include_package_data=True,
)
