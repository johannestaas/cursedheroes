cursedheroes
============

A turn-based strategy game in your terminal!

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ cursedheroes

Use --help/-h to view info on the arguments::

    $ cursedheroes --help

Release Notes
-------------

:0.0.1:
    Project created
